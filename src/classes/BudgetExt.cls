public with sharing class BudgetExt {
    
/*************************************************
 * Controller extension to provide functionality required for the Budgets
 * section of Contact detail page.  The business requirement was to allow
 * the Budgets related list on Contact to display 14 columns (year
 * + 12 months + total).  The end result is an embedded VF page component on the Contact
 * page layout that displays the Budget related rows in view mode 
 * (read only), with an edit button local to the component.  Clicking Edit 
 * switches the component to Edit Mode by changing the PageReference to
 * the related Edit mode VF page.  On that page are three buttons (Add Row, 
 * Save and Cancel) plus a Delete button on each row.  Hitting Save or Cancel
 * brings you back to the View mode page.
 * 
 * Related VF pages:
 * - BudgetEdit.vfp
 * - BudgetView.vfp
 * 
 * Author: Charles Thompson, Salesforce, Nov 2017
 * *************************************************/
    
    private final Id contactID; // store the parent (Contact) ID
    private ApexPages.StandardController sc;
    public Boolean refreshPage {get; set;}
    public Integer index {get;set;} // store the current row being deleted
    
	// List to represent each row of the pageBlockTable of Budgets 
	// related to the parent Sub Project
    public List<Budget__c> budgets {get; set;}

    // List to hold those rows being deleted
	public List<Budget__c> budgetsToDel = new List<Budget__c>();
    
    // Variable to hold the Sub Project (parent) record
    public Contact theContact {get; set;}
    
    // Initial load of the pageBlockTable
    public BudgetExt(ApexPages.StandardController stdController){
        sc = stdController;
        refreshPage = false;
        contactID = stdController.getId();
        this.index = 0;
        theContact = [SELECT Id
                      FROM   Contact
                      WHERE  Id = :contactID
                      LIMIT 1
                     ];
        budgets =    [SELECT Id,
                             Year__c,
              		         Jan__c, Feb__c, Mar__c, Apr__c,
              		         May__c, Jun__c, Jul__c, Aug__c,
             		         Sep__c, Oct__c, Nov__c, Dec__c,
                             Year_Total__c
                      FROM   Budget__c
                      WHERE  Contact__c = :contactID
               ORDER BY Year__c
              ];
    }
                              
    public String getIndex(){
        return String.valueOf(index);
    }
    
    public void setIndex(String indexIn){
        this.index = Integer.valueOf(indexIn);
    }
            
	// Add an empty budget row to the table 
	// (not saved to the database until saveBuds() is called)
    public void addRow(){
        Budget__c row = new Budget__c();
        row.Contact__c = contactID;
        budgets.add(row); 
    }
    
    // delete a forecast from the table (not saved to the db until later)
    public void delRow(){
        
        Integer i = this.index-1; // Rows start at one, Lists start at zero
        
        // If the row is an existing row then add it to the list 
        // to delete from the database
        if (budgets[i].Id != null){
            budgetsToDel.add(budgets[i]);
        }
        //Remove the forecast from the table    
        budgets.remove(i);
    }
    
    public PageReference saveBuds(){

        // prepare to save the data in all the rows
        for (Budget__c bud: budgets){
            // blank = 0
            if (bud.Jan__c == null) {bud.Jan__c = 0;}
            if (bud.Feb__c == null) {bud.Feb__c = 0;}
            if (bud.Mar__c == null) {bud.Mar__c = 0;}
            if (bud.Apr__c == null) {bud.Apr__c = 0;}
            if (bud.May__c == null) {bud.May__c = 0;}
            if (bud.Jun__c == null) {bud.Jun__c = 0;}
            if (bud.Jul__c == null) {bud.Jul__c = 0;}
            if (bud.Aug__c == null) {bud.Aug__c = 0;}
            if (bud.Sep__c == null) {bud.Sep__c = 0;}
            if (bud.Oct__c == null) {bud.Oct__c = 0;}
            if (bud.Nov__c == null) {bud.Nov__c = 0;}
            if (bud.Dec__c == null) {bud.Dec__c = 0;} 
            
            //**** UNCOMMENT below if using multi-currency ************
            //
            // ensure the currency follows through from the Contact
            //if (bud.CurrencyIsoCode != theContact.CurrencyIsoCode){
            //    bud.CurrencyIsoCode = theContact.CurrencyIsoCode;
            //}
            //*********************************************************
        }
        
        // update and insert forecasts as per the current list
        upsert budgets;
        
        // delete any rows in the list of deletions
        if ((budgetsToDel != null) && (budgetsToDel.size() > 0)){ 
            delete budgetsToDel;
        }
        
        // switch back to View mode and refresh the whole page
        refreshPage = true;
        sc.save(); // save the Contact page to ensure formulas are updated
        return null;
    }
    
    public PageReference editBuds(){
        	// switch to Edit mode
        	PageReference editPage = new PageReference('/apex/BudgetEdit?id=' + contactID);
        return editPage;
    }

    public PageReference cancelEdit(){
        	// switch back to View mode without saving anything
        	PageReference viewPage = new PageReference('/apex/BudgetView?id=' + contactID);
        viewPage.setRedirect(true);
        	return viewPage;
    }   
}